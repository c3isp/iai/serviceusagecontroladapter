package iit.cnr.it.types;

import org.springframework.http.ResponseEntity;

public enum Clock {
	CLOCK;

	private int timeWindow;
	private int missedHeartbeat;
	private boolean isDead;
	private int maxMissedHeartbeat;
	private int deadCount;
	private String heartbeatMessage;
	private ResponseEntity<String> response;
	private boolean hasHeartbeatArrived;
	private static Object threadLock = new Object();
	private static Object apiLock = new Object();

	private Clock() {
	}

	public int getTimeWindow() {
		return timeWindow;
	}

	public void setTimeWindow(int timeWindow) {
		this.timeWindow = timeWindow;
	}

	public int getMissedHeartbeat() {
		return missedHeartbeat;
	}

	public void setMissedHeartbeat(int missedHeartbeat) {
		this.missedHeartbeat = missedHeartbeat;
	}

	public boolean isDead() {
		return isDead;
	}

	public void setDead(boolean isDead) {
		this.isDead = isDead;
	}

	public int getMaxMissedHeartbeat() {
		return maxMissedHeartbeat;
	}

	public void setMaxMissedHeartbeat(int maxMissedHeartbeat) {
		this.maxMissedHeartbeat = maxMissedHeartbeat;
	}

	public int getDeadCount() {
		return deadCount;
	}

	public void setDeadCount(int deadCount) {
		this.deadCount = deadCount;
	}

	public String getHeartbeatMessage() {
		return heartbeatMessage;
	}

	public void setHeartbeatMessage(String heartbeatMessage) {
		this.heartbeatMessage = heartbeatMessage;
	}

	public ResponseEntity<String> getResponse() {
		return response;
	}

	public void setResponse(ResponseEntity<String> response) {
		this.response = response;
	}

	public boolean isHasHeartbeatArrived() {
		return hasHeartbeatArrived;
	}

	public void setHasHeartbeatArrived(boolean hasHeartbeatArrived) {
		this.hasHeartbeatArrived = hasHeartbeatArrived;
	}

	public static Object getThreadLock() {
		return threadLock;
	}

	public static void setThreadLock(Object threadLock) {
		Clock.threadLock = threadLock;
	}

	public static Object getApiLock() {
		return apiLock;
	}

	public static void setApiLock(Object apiLock) {
		Clock.apiLock = apiLock;
	}

}
