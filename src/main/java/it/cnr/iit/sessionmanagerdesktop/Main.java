/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.cnr.iit.sessionmanagerdesktop;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
//import it.cnr.iit.sessionmanagerdalutilities.Session;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author fabio
 */
public class Main {
  /**
   * @param args
   *          the command line arguments
   * @throws SQLException
   */
  public static void main(String[] args) throws SQLException {
    String connection = args[0];
    System.out.println(connection);
    if (connection.length() > 4) {
      System.out.println("OLD: " + connection);
      SessionManagerDesktop sm = new SessionManagerDesktop(
          connection);
      // SessionManager sm = new
      // SessionManager("jdbc:postgresql://146.48.125.102:5432/session_manager?user=ucon&password=xxx");
      sm.start();

      List<String> attributes = new LinkedList<>();
      attributes.add("aaa");
      attributes.add("bbb");
    } else {
      Connection conn = null;
      Properties connectionProps = new Properties();
      connectionProps.put("user", "root");
      connectionProps.put("password", "root");

      conn = DriverManager.getConnection(
          "jdbc:" + "mysql" + "://" +
              "localhost" +
              ":" + "3306" + "/",
          connectionProps);
      System.out.println("Connected to database");
    }
    // ESEMPIO 1: CREA TRE SESSIONI, AGGIORNA LO STATO DI UNA, TESTA I METODI
    // GETSESSIONSFORSTATUS E GETSESSIONFOR ID E LE ELIMINA
    /*
     * sm.createEntryForSubject("1", "<policyOnGoing1>", "<policyPost1>",
     * "<originalRequest1>", attributes, "status1", "pepURI1", "fabio");
     * sm.createEntryForObject("2", "<policyOnGoing1>", "<policyPost1>",
     * "<originalRequest1>", attributes, "status1", "pepURI1", "room");
     * sm.createEntryForObject("3", "<policyOnGoing1>", "<policyPost1>",
     * "<originalRequest1>", attributes, "status1", "pepURI1", "desk");
     * sm.updateEntry("1", "status2");
     * 
     * List<Session> result = sm.getSessionsForStatus("status1"); for (Session s
     * : result) { System.out.println(s.toString()); }
     */

    // System.out.println(sm.getSessionForId("1").toString());

    // sm.deleteEntry("1");
    // sm.deleteEntry("2");
    // sm.deleteEntry("3");

    // ESEMPIO 2: CREA 3 SESSIONI CON ON_GOING_ATTRIBUTES MULTI-VALORE E
    // RICHIAMA LA GETSESSIONSFOROBJECTSATTRIBUTES INVIANDOLE 1 ATTRIBUTO
    /*
     * sm.createEntryForSubject("1", "<policyOnGoing1>", "<policyPost1>",
     * "<originalRequest1>", attributes, "status1", "pepURI1", "fabio");
     * sm.createEntryForObject("2", "<policyOnGoing1>", "<policyPost1>",
     * "<originalRequest1>", attributes, "status1", "pepURI1", "room");
     * sm.createEntryForObject("3", "<policyOnGoing1>", "<policyPost1>",
     * "<originalRequest1>", attributes, "status1", "pepURI1", "desk");
     * sm.createEntryForObject("4", "<policyOnGoing1>", "<policyPost1>",
     * "<originalRequest1>", attributes, "status1", "pepURI1", "desk");
     * List<Session> result = sm.getSessionsForObjectAttributes("aaa"); for
     * (Session s : result) { System.out.println(s.toString()); } List<Session>
     * result1 = sm.getSessionsForObjectAttributes("desk","aaa"); for (Session s
     * : result1) { System.out.println(s.toString()); }
     */

    // ESEMPIO 3: CREA 1 SESSIONE CON ON_GOING_ATTRIBUTES AVENTI SIA SUBJECT CHE
    // OBJECT E POI LI AGGIORNA
    /*
     * sm.createEntry("3", "<policySet>", "<originalRequest1>", attributes,
     * "status1", "pepURI1", "desk", "fabio");
     * sm.updateObjectForSessionAndAttribute("3", "aaa", "desk");
     * sm.updateSubjectForSessionAndAttribute("3", "aaa","fabio");
     * sm.updateObjectForSessionAndAttribute("3", "bbb", "desk");
     * sm.updateSubjectForSessionAndAttribute("3", "bbb","fabio");
     */
  }
}
