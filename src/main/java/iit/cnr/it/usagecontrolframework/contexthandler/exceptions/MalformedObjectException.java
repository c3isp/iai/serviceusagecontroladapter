package iit.cnr.it.usagecontrolframework.contexthandler.exceptions;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MalformedObjectException extends Exception {
	
	private final Logger LOGGER = Logger
	    .getLogger(MalformedObjectException.class.getName());
	
	public MalformedObjectException(String message) {
		super(message);
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public void printStackTrace() {
		LOGGER.log(Level.SEVERE, getMessage());
	}
	
}
