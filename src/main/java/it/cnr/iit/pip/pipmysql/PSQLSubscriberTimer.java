package it.cnr.iit.pip.pipmysql;

import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import iit.cnr.it.ucsinterface.contexthandler.ContextHandlerPIPInterface;
import iit.cnr.it.ucsinterface.message.PART;
import iit.cnr.it.ucsinterface.message.remoteretrieval.MessagePipCh;
import iit.cnr.it.ucsinterface.message.remoteretrieval.PipChContent;
import iit.cnr.it.ucsinterface.pip.exception.PIPException;
import iit.cnr.it.xacmlutilities.Attribute;
import it.cnr.iit.pip.pipmysql.tables.UserAttributes;
import it.cnr.iit.sqlmiddlewareinterface.SQLMiddlewarePIPInterface;

/**
 * Subscriber timer for the PIPReader.
 * <p>
 * Basically the subscriber timer is in hcarge of performing the task of
 * refreshing periodically the value of a certain attribute, if that value
 * changes, then it has to update the value in the subscriptions queue.
 *
 * <p>
 * By removing the public attribute to this class we have allowed only classes
 * in the same package to create or instantiate such a class
 * </p>
 *
 * @author antonio
 *
 */

final class PSQLSubscriberTimer extends TimerTask {

	// logger to be used to log the actions
	private Logger LOGGER = Logger.getLogger(PSQLSubscriberTimer.class.getName());

	// the queue of attributes that have been subscribed
	private final BlockingQueue<Attribute> subscriptions;

	// the interface to communicate with the context handler
	private ContextHandlerPIPInterface contextHandler;

	private SQLMiddlewarePIPInterface sqlInterface;

	PIPUserAttributes pip;

	/**
	 * Constructor for a new Subscriber timer
	 *
	 * @param contextHandler the interface to the context handler
	 * @param map            the list of attributes that have been subscribed
	 * @param path           the path to the file to be read
	 */
	PSQLSubscriberTimer(ContextHandlerPIPInterface contextHandler, BlockingQueue<Attribute> map,
			SQLMiddlewarePIPInterface sqlInterface, PIPUserAttributes pip) {
		subscriptions = map;
		this.sqlInterface = sqlInterface;
		this.pip = pip;
		for (Attribute entry : subscriptions) {
			System.out.println("Subscriber Timer MYSQL: " + entry.getAttributeId());

		}
	}

	@Override
	public void run() {

		for (Attribute entry : subscriptions) {
			// Category category = entry.getCategory();
			String newValue = "";
			newValue = read(entry);
			// if the attribute has not changed
			if (entry.getAttributeValues(entry.getAttributeDataType()).get(0).equals(newValue)) {
				;
			} else {
				LOGGER.log(Level.INFO, "[TIME] value of the attribute changed at " + System.currentTimeMillis() + "\t"
						+ newValue + "\t" + entry.getAdditionalInformations());
				entry.setValue(entry.getAttributeDataType(), newValue);
				PipChContent pipChContent = new PipChContent();
				pipChContent.addAttribute(entry);
				MessagePipCh messagePipCh = new MessagePipCh(PART.PIP.toString(), PART.CH.toString());
				messagePipCh.setMotivation(pipChContent);
				contextHandler.attributeChanged(messagePipCh);
			}
		}
		return;
	}

	/**
	 * Effective retrieval of the monitored value, before this retrieval many checks
	 * may have to be performed
	 *
	 * @return the requested string
	 * @throws PIPException
	 */
	private String read(Attribute attribute) {
		UserAttributes userAttributes = pip.readMysql(pip.filter);
		if (attribute.getAttributeId().contains("organisation")) {
			try {
				LOGGER.log(Level.INFO, "[PIPUserAttributes] AttributeId: " + attribute.getAttributeId() + " for "
						+ pip.filter + " Value:" + userAttributes.getOrganizationName());
				return userAttributes.getOrganizationName();
			} catch (Exception e) {
				return "";
			}
		}
		try {
			if (attribute.getAttributeId().contains("role")) {

				LOGGER.log(Level.INFO, "[PIPUserAttributes] AttributeId: " + attribute.getAttributeId() + " for "
						+ pip.filter + " Value:" + userAttributes.getRole());
				return userAttributes.getRole();

			}
		} catch (Exception e) {
			return "";
		}

		if (attribute.getAttributeId().contains("ismemberof")) {
			LOGGER.log(Level.INFO, "[PIPUserAttributes] AttributeId: " + attribute.getAttributeId() + " for "
					+ pip.filter + " Value:" + userAttributes.getGroup());
			return userAttributes.getGroup();

		}
		if (attribute.getAttributeId().contains("country")) {
			LOGGER.log(Level.INFO, "[PIPUserAttributes] AttributeId: " + attribute.getAttributeId() + " for "
					+ pip.filter + " Value:" + userAttributes.getCountry());

			return userAttributes.getCountry();

		}
		return "";
	}

	/**
	 * Sets the context handler interface
	 *
	 * @param contextHandler
	 */
	public void setContextHandlerInterface(ContextHandlerPIPInterface contextHandler) {
		// BEGIN parameter checking
		if (contextHandler == null) {
			LOGGER.log(Level.SEVERE, "Context handler is null");
			return;
		}
		// END parameter checking
		this.contextHandler = contextHandler;
	}

	public ContextHandlerPIPInterface getContextHandler() {
		return contextHandler;
	}

}
