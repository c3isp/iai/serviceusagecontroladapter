package it.cnr.iit.pip.pipmysql;

import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import iit.cnr.it.ucsinterface.obligationmanager.ObligationInterface;
import iit.cnr.it.ucsinterface.pip.PIPBase;
import iit.cnr.it.ucsinterface.pip.exception.PIPException;
import iit.cnr.it.usagecontrolframework.configuration.xmlclasses.XMLAttribute;
import iit.cnr.it.usagecontrolframework.configuration.xmlclasses.XMLPip;
import iit.cnr.it.xacmlutilities.Attribute;
import iit.cnr.it.xacmlutilities.Category;
import iit.cnr.it.xacmlutilities.DataType;
import iit.cnr.it.xacmlutilities.policy.utility.JAXBUtility;
import it.cnr.iit.pip.pipmysql.tables.UserAttributes;
import oasis.names.tc.xacml.core.schema.wd_17.RequestType;

public class PIPUserAttributes extends PIPBase {

	/**
	 * Whenever a PIP has to retrieve some informations related to an attribute that
	 * is stored inside the request, it has to know in advance all the informations
	 * to retrieve that atrtribute. E.g. if this PIP has to retrieve the
	 * informations about the subject, it has to know in advance which is the
	 * attribute id qualifying the subject, its category and the datatype used,
	 * otherwise it is not able to retrieve the value of that attribute, hence it
	 * would not be able to communicate with the AM properly
	 */
	private Category expectedCategory;

	private boolean initialized = false;

	String filter;
	UserAttributes userAttributes;
	String pipId = "";

	private String databaseURL;
	private ConnectionSource connection;

	// list that stores the attributes on which a subscribe has been performed
	protected final BlockingQueue<Attribute> subscriptions = new LinkedBlockingQueue<>();

	private final static Logger LOGGER = Logger.getLogger(PIPUserAttributes.class.getName());

	private Dao<UserAttributes, String> daoUser;
	private Timer timer = new Timer();

	private PSQLSubscriberTimer subscriberTimer;

	public PIPUserAttributes(String xmlPip) {
		super(xmlPip);
		System.out.println("XML PIP: " + xmlPip);
		pipId = xmlPip.split("pip id=")[1].split("class")[0].split("-")[1];
		pipId = pipId.replace("\"", "");
		System.out.println("PIP ID: " + pipId);

		if (!super.isInitialized()) {

		}
		if (initialize(xmlPip)) {
			System.out.println("SubscriberTimer initialization");
			subscriberTimer = new PSQLSubscriberTimer(contextHandlerInterface, subscriptions, null, this);
			timer.scheduleAtFixedRate(subscriberTimer, 0, 10 * 1000);
		}
	}

	/**
	 * Initializes the various fields of this PIP
	 *
	 * @param xmlPip the xml configuration
	 * @return true if everything goes fine, false otherwise
	 */
	private boolean initialize(String xmlPip) {
		try {
			XMLPip xml = JAXBUtility.unmarshalToObject(XMLPip.class, xmlPip);
			List<XMLAttribute> attributes = xml.getAttributes();
			DatabaseConfiguration configurationInterface = DatabaseConfiguration.createDBConfiguration(xml);
			databaseURL = configurationInterface.getURL();
			connection = new JdbcPooledConnectionSource(databaseURL);
			for (XMLAttribute xmlAttribute : attributes) {
				Map<String, String> arguments = xmlAttribute.getArgs();
				Attribute attribute = new Attribute();
				if (!attribute.createAttributeId(arguments.get(ATTRIBUTE_ID))) {
					LOGGER.log(Level.SEVERE, "[PIPMYSQL] wrong set Attribute");
					return false;
				}
				if (!attribute.setCategory(Category.toCATEGORY(arguments.get(CATEGORY)))) {
					LOGGER.log(Level.SEVERE, "[PIPReader] wrong set category " + arguments.get(CATEGORY));
					return false;
				}
				if (!attribute.setAttributeDataType(DataType.toDATATYPE(arguments.get(DATA_TYPE)))) {
					LOGGER.log(Level.SEVERE, "[PIPMYSQL] wrong set datatype");
					return false;
				}
				if (attribute.getCategory() != Category.ENVIRONMENT) {
					if (!setExpectedCategory(arguments.get(EXPECTED_CATEGORY))) {
						return false;
					}
				}
				addAttribute(attribute);
			}
			daoUser = DaoManager.createDao(connection, UserAttributes.class);
			initialized = true;
			return true;
		} catch (

		Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	// Add the attributes in the subscriber list
	@Override
	public void subscribe(RequestType accessRequest) throws PIPException {
		LOGGER.log(Level.SEVERE, "[PIPMYSQL] SUBSCRIBE");

		// BEGIN parameter checking
		if (accessRequest == null || !initialized || !isInitialized()) {
			LOGGER.log(Level.SEVERE, "[PIPREader] wrong initialization" + initialized + "\t" + isInitialized());
			return;
		}
		// END parameter checking

		subscriberTimer.setContextHandlerInterface(contextHandlerInterface);

		if (subscriberTimer.getContextHandler() == null || contextHandlerInterface == null) {
			LOGGER.log(Level.SEVERE, "Context handler not set");
			return;
		}

		// Get subject ID
		filter = accessRequest.extractValue(Category.SUBJECT);
		System.out.println("filter=" + filter);
		// Retrieve user attributes from MYSQL
		userAttributes = readMysql(filter);
		System.out.println(pipId);
		retrieve(accessRequest);

		for (Attribute attribute : getAttributes()) {
			System.out.println("Attribute size Subscribe: " + getAttributeIds().size());
			System.out.println("Attribute name Subscribe: " + attribute.getAttributeId());
			if (attribute.getAttributeId().contains("organisation")) {
				attribute.setValue(attribute.getAttributeDataType(), userAttributes.getOrganizationName());
				attribute.setAdditionalInformations(filter);

				if (!subscriptions.contains(attribute)) {
					subscriptions.add(attribute);
				}

			}
			if (attribute.getAttributeId().contains("role")) {
				attribute.setValue(attribute.getAttributeDataType(), userAttributes.getRole());
				attribute.setAdditionalInformations(filter);

				if (!subscriptions.contains(attribute)) {
					subscriptions.add(attribute);
				}
			}
			if (attribute.getAttributeId().contains("ismemberof")) {
				attribute.setValue(attribute.getAttributeDataType(), userAttributes.getGroup());
				attribute.setAdditionalInformations(filter);

				if (!subscriptions.contains(attribute)) {
					subscriptions.add(attribute);
				}
			}
			if (attribute.getAttributeId().contains("country")) {
				attribute.setValue(attribute.getAttributeDataType(), userAttributes.getCountry());
				attribute.setAdditionalInformations(filter);

				if (!subscriptions.contains(attribute)) {
					subscriptions.add(attribute);
				}
			}
		}

	}

	// Called when a tryAccess occurs
	@Override
	public void retrieve(RequestType accessRequest) throws PIPException {

		System.out.println("Retrieve");
		// BEGIN parameter checking
		if (accessRequest == null || !initialized || !isInitialized()) {
			LOGGER.log(Level.SEVERE, "[PIPREader] wrong initialization" + initialized + "\t" + isInitialized());
			return;
		}

		// Get subject ID
		filter = accessRequest.extractValue(Category.SUBJECT);
		// Retrieve user attributes from MYSQL
		userAttributes = readMysql(filter);
		System.out.println("filter=" + filter);
		// Read attributes from request and fill the request with the attributes read
		for (Attribute attribute : getAttributes()) {

			if (attribute.getAttributeId().contains("organisation")) {

				try {
					LOGGER.log(Level.INFO, "[PIPUserAttributes] AttributeId: " + attribute.getAttributeId() + " for "
							+ filter + " Value:" + userAttributes.getOrganizationName());

					accessRequest.addAttribute(attribute.getCategory().toString(),
							attribute.getAttributeDataType().toString().toString(), attribute.getAttributeId(),
							userAttributes.getOrganizationName());
				} catch (Exception e) {
					accessRequest.addAttribute(attribute.getCategory().toString(),
							attribute.getAttributeDataType().toString().toString(), attribute.getAttributeId(), "");
				}
			}
			if (attribute.getAttributeId().contains("role")) {
				try {

					LOGGER.log(Level.INFO, "[PIPUserAttributes] AttributeId: " + attribute.getAttributeId() + " for "
							+ filter + " Value:" + userAttributes.getRole());

					accessRequest.addAttribute(attribute.getCategory().toString(),
							attribute.getAttributeDataType().toString().toString(), attribute.getAttributeId(),
							userAttributes.getRole());
				} catch (Exception e) {
					accessRequest.addAttribute(attribute.getCategory().toString(),
							attribute.getAttributeDataType().toString().toString(), attribute.getAttributeId(), "");
				}
			}
			if (attribute.getAttributeId().contains("ismemberof")) {
				try {

					LOGGER.log(Level.INFO, "[PIPUserAttributes] AttributeId: " + attribute.getAttributeId() + " for "
							+ filter + " Value:" + userAttributes.getGroup());

					accessRequest.addAttribute(attribute.getCategory().toString(),
							attribute.getAttributeDataType().toString().toString(), attribute.getAttributeId(),
							userAttributes.getGroup());
				} catch (Exception e) {
					accessRequest.addAttribute(attribute.getCategory().toString(),
							attribute.getAttributeDataType().toString().toString(), attribute.getAttributeId(), "");
				}
			}
			if (attribute.getAttributeId().contains("country")) {
				try {

					LOGGER.log(Level.INFO, "[PIPUserAttributes] AttributeId: " + attribute.getAttributeId() + " for "
							+ filter + " Value:" + userAttributes.getCountry());

					accessRequest.addAttribute(attribute.getCategory().toString(),
							attribute.getAttributeDataType().toString().toString(), attribute.getAttributeId(),
							userAttributes.getCountry());
				} catch (Exception e) {
					accessRequest.addAttribute(attribute.getCategory().toString(),
							attribute.getAttributeDataType().toString().toString(), attribute.getAttributeId(), "");
				}
			}
		}
	}

	final private boolean setExpectedCategory(String category) {
		// BEGIN parameter checking
		if (!isInitialized() || category == null || category.isEmpty()) {
			initialized = false;
			return false;
		}
		// END parameter checking
		Category categoryObj = Category.toCATEGORY(category);
		if (categoryObj == null) {
			initialized = false;
			return false;
		}
		expectedCategory = categoryObj;
		return true;
	}

	public UserAttributes readMysql(String string) {
		UserAttributes user = null;
		try {
			QueryBuilder<UserAttributes, String> qbAttributes = daoUser.queryBuilder();
			List<UserAttributes> attributes = qbAttributes.where().eq("username", string).query();
			if (attributes.size() > 0) {
				user = attributes.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return user;

	}

	@Override
	public void unsubscribe(List<Attribute> attributes) throws PIPException {
		System.out.println("PIPMYSQL unsubscribe");
		for (Attribute attribute : attributes) {
			System.out.println("attribute to remove: " + attribute);
			subscriptions.remove(attribute);
		}
		System.out.println("Subscription length: " + subscriptions.size());

	}

	@Override
	public String retrieve(Attribute attributeRetrievals) throws PIPException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String subscribe(Attribute attributeRetrieval) throws PIPException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void retrieve(RequestType request, List<Attribute> attributeRetrievals) {
		// TODO Auto-generated method stub

	}

	@Override
	public void subscribe(RequestType request, List<Attribute> attributeRetrieval) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAttribute(String json) throws PIPException {
		// TODO Auto-generated method stub

	}

	@Override
	public void performObligation(ObligationInterface obligation) {
		// TODO Auto-generated method stub

	}

}
