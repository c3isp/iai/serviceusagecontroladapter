package iit.cnr.it.usagecontrolframework.entry;

public final class CONSTANTS {
	private CONSTANTS() {
		
	}
	
	public static final String	TRUE	= "TRUE";
	public static final String	FALSE	= "FALSE";
}
