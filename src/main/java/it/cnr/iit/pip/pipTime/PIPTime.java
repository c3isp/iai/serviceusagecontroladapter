/*
 *
 * @authors Giacomo Giorgi
 */
package it.cnr.iit.pip.pipTime;


import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;
import iit.cnr.it.ucsinterface.obligationmanager.ObligationInterface;
import iit.cnr.it.ucsinterface.pip.PIPBase;
import iit.cnr.it.ucsinterface.pip.exception.PIPException;
import iit.cnr.it.usagecontrolframework.configuration.xmlclasses.XMLAttribute;
import iit.cnr.it.usagecontrolframework.configuration.xmlclasses.XMLPip;
import iit.cnr.it.xacmlutilities.Attribute;
import iit.cnr.it.xacmlutilities.Category;
import iit.cnr.it.xacmlutilities.DataType;
import iit.cnr.it.xacmlutilities.policy.utility.JAXBUtility;
import oasis.names.tc.xacml.core.schema.wd_17.RequestType;

/**
 * PIP Time
 * The Pip Time
 * @author Giacomo Giorgi
 */
public class PIPTime extends PIPBase {

	private final static Logger LOGGER = Logger.getLogger(PIPTime.class.getName());

	protected final BlockingQueue<Attribute> subscriptions = new LinkedBlockingQueue<>();
	private TimeSubscriberTimer subscriberTimer;
	private volatile boolean initialized = false;
	private Category expectedCategory;

	/*
	private LDAPConnector ldapConnector = null;
	*/
	private Timer timer = new Timer();

	/**
	 * Initialization of the PIP Time and Timer subscription
	 * @param xmlPip
	 */
	public PIPTime(String xmlPip) {

		super(xmlPip);
		LOGGER.log(Level.INFO, "[PIPTime] constructor");

		if (!isInitialized()) {
			return;
		}
		if (initialize(xmlPip)) {
			LOGGER.log(Level.INFO, "[PIPTime] initialize");

			initialized = true;
			subscriberTimer = new TimeSubscriberTimer(contextHandlerInterface, subscriptions, getAttributeIds());
			timer.scheduleAtFixedRate(subscriberTimer, 0, 10 * 1000);
		} else {
			return;
		}
	}

	private boolean initialize(String xmlPIP) {
		try {

			XMLPip xmlPip = JAXBUtility.unmarshalToObject(XMLPip.class, xmlPIP);
			List<XMLAttribute> attributes = xmlPip.getAttributes();
			for (XMLAttribute xmlAttribute : attributes) {

				Map<String, String> arguments = xmlAttribute.getArgs();
				Attribute attribute = new Attribute();
				
				LOGGER.log(Level.INFO, "[PIPTime] attributes " + attribute.createAttributeId(arguments.get(ATTRIBUTE_ID)));

				if (!attribute.createAttributeId(arguments.get(ATTRIBUTE_ID))) {
					LOGGER.log(Level.SEVERE, "[PIPTime] wrong set Attribute");
					return false;
				}
				if (!attribute.setCategory(Category.toCATEGORY(arguments.get(CATEGORY)))) {
					LOGGER.log(Level.SEVERE, "[PIPTime] wrong set category " + arguments.get(CATEGORY));
					return false;
				}
				if (!attribute.setAttributeDataType(DataType.toDATATYPE(arguments.get(DATA_TYPE)))) {
					LOGGER.log(Level.SEVERE, "[PIPTime] wrong set datatype");
					return false;
				}
				if (attribute.getCategory() != Category.ENVIRONMENT) {
					
					if (!setExpectedCategory(arguments.get(EXPECTED_CATEGORY))) {
						return false;
					}
				}

				
				addAttribute(attribute);
			}
			initialized = true;

			return true;
		} catch (JAXBException e) {
			e.printStackTrace();
			return false;
		}
	}



	final private boolean setExpectedCategory(String category) {
		// BEGIN parameter checking
		if (!isInitialized() || category == null || category.isEmpty()) {
			initialized = false;
			return false;
		}
		// END parameter checking
		Category categoryObj = Category.toCATEGORY(category);
		if (categoryObj == null) {
			initialized = false;
			return false;
		}
		expectedCategory = categoryObj;
		return true;
	}

	

	@Override
	public void unsubscribe(List<Attribute> attributes) throws PIPException {
		// TODO Auto-generated method stub

	}

	/**
	 * Retrieves the attribute values (Current Date) to insert in the access Request
	 * 
	 * @param request a XACML file where the user will be retrieved
	 * @throws PIPException
	 */
	@Override
	public void retrieve(RequestType accessRequest) throws PIPException {
		LOGGER.log(Level.INFO, "[PIPTime] Retrieve attributes ");


		for (String attributeId : getAttributeIds()) {
			LOGGER.log(Level.INFO, "[PIPTime] Retrieve attributes " + attributeId);
			if (attributeId.contains("date")) {
				Attribute attribute = new Attribute();
				attribute.createAttributeId(attributeId);
				attribute.setAttributeDataType(DataType.DATE);
				attribute.setCategory(Category.ENVIRONMENT);
			    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
			    LocalDateTime now = LocalDateTime.now();  
				attribute.setValue(DataType.DATE, dtf.format(now).toString());
				accessRequest.addAttribute(Category.ENVIRONMENT.toString(), DataType.DATE.toString(),
						attribute.getAttributeId(), attribute.getAttributeValues(DataType.DATE));
			}
			if (attributeId.contains("time")) {
				Attribute attribute = new Attribute();
				attribute.createAttributeId(attributeId);
				attribute.setAttributeDataType(DataType.TIME);
				attribute.setCategory(Category.ENVIRONMENT);
				LocalTime time = LocalTime.now(); 
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
				System.out.println(time.format(formatter).toString());
				attribute.setValue(DataType.TIME, time.format(formatter).toString());
				accessRequest.addAttribute(Category.ENVIRONMENT.toString(), DataType.TIME.toString(),
						attribute.getAttributeId(), attribute.getAttributeValues(DataType.TIME));
			}

		}

		return;
	}
	@Override
	public String subscribe(Attribute attributeRetrieval) throws PIPException {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public void subscribe(RequestType request, List<Attribute> attributeRetrieval) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAttribute(String json) throws PIPException {
		// TODO Auto-generated method stub

	}

	@Override
	public void performObligation(ObligationInterface obligation) {
		// TODO Auto-generated method stub

	}

	@Override
	public String retrieve(Attribute arg0) throws PIPException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void retrieve(RequestType arg0, List<Attribute> arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void subscribe(RequestType arg0) throws PIPException {
		// TODO Auto-generated method stub
		
	}

}
