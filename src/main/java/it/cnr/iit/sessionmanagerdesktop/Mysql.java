package it.cnr.iit.sessionmanagerdesktop;

import java.sql.Connection;
import java.sql.DriverManager;

public class Mysql {
  public static void main(String[] args) throws Exception {
    Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
    Connection conn = null;
    // Properties connectionProps = new Properties();
    // connectionProps.put("user", "root");
    // connectionProps.put("password", "root");

    // conn = DriverManager.getConnection(
    // "jdbc:" + "mysql" + "://" +
    // "localhost" +
    // ":" + "3306" + "/",
    // connectionProps);
    DriverManager.getConnection("jdbc:mysql://localhost/session_manager?" +
        "user=root&password=root");
    System.out.println("Connected to database");
  }
}
