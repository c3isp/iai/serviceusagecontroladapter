package iit.cnr.it.usagecontrolframework.pdp;

public enum RULE_COMBINING {
	DENY_UNLESS_PERMIT(
	    "urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:deny-unless-permit"), FIRST_APPLICABLE(
	        "urn:oasis:names:tc:xacml:1.0:rule-combining-algorithm:first-applicable");
	
	String value;
	
	private RULE_COMBINING(String string) {
		value = string;
	}
	
	public String getValue() {
		return value;
	}
}
