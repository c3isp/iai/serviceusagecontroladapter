package it.cnr.iit.pipdsa;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import iit.cnr.it.ucsinterface.obligationmanager.ObligationInterface;
import iit.cnr.it.ucsinterface.pip.PIPBase;
import iit.cnr.it.ucsinterface.pip.exception.PIPException;
import iit.cnr.it.usagecontrolframework.configuration.xmlclasses.XMLAttribute;
import iit.cnr.it.usagecontrolframework.configuration.xmlclasses.XMLPip;
import iit.cnr.it.xacmlutilities.Attribute;
import iit.cnr.it.xacmlutilities.Category;
import iit.cnr.it.xacmlutilities.DataType;
import iit.cnr.it.xacmlutilities.policy.utility.JAXBUtility;
import oasis.names.tc.xacml.core.schema.wd_17.AttributeType;
import oasis.names.tc.xacml.core.schema.wd_17.AttributesType;
import oasis.names.tc.xacml.core.schema.wd_17.RequestType;

/**
 * This is a PIPDsa.
 * <p>
 * The PIPDsa is very C3ISP specific since it is the PIP in charge of verifying
 * the characteristics of DSA:
 * <ol>
 * <li>The version of the DSA</li>
 * <li>The status of the DSA</li>
 * <li>Temporarily there will be also the part related to CoCoCloud in which we
 * used to have RuntimeAttestation</li>
 * </ol>
 * All the informations related to these attributes come from the configuration
 * of this PIP. The PIP expects to have the DSA_ID (conversely the PolicyID as
 * an attribute of the resource that is requested). <b>This attributeID has
 * multiple values</b>
 * </p>
 * 
 * @author antonio
 *
 */
final public class PIPDsa extends PIPBase {

  // ---------------------------------------------------------------------------
  // Class attributes
  // ---------------------------------------------------------------------------
  /**
   * Whenever a PIP has to retrieve some informations related to an attribute
   * that is stored inside the request, it has to know in advance all the
   * informations to retrieve that atrtribute. E.g. if this PIP has to retrieve
   * the informations about the subject, it has to know in advance which is the
   * attribute id qualifying the subject, its category and the datatype used,
   * otherwise it is not able to retrieve the value of that attribute, hence it
   * would not be able to communicate with the AM properly
   */
  private Category expectedCategory;

  // this is the attribute manager of this pip
  private String filePath;

  // states if the pip has been correctly initialized
  private volatile boolean initialized = false;

  private String attributeIdFilter;

  // list that stores the attributes on which a subscribe has been performed
  protected final BlockingQueue<Attribute> subscriptions = new LinkedBlockingQueue<>();

  // logger
  private Logger LOGGER = Logger.getLogger(PIPDsa.class.getName());

  // the subscriber timer in charge of performing the polling of the values
  private PRSubscriberTimer subscriberTimer;
  // timer to be used to instantiate the subscriber timer
  private Timer timer = new Timer();

  // ---------------------------------------------------------------------------
  // CONSTRUCTOR
  // ---------------------------------------------------------------------------
  /**
   * Constructor for the PIP reader
   * 
   * @param xmlPip
   *          the xml describing the pipreader in string format
   */
  public PIPDsa(String xmlPip) {
    super(xmlPip);
    if (!isInitialized()) {
      return;
    }
    if (initialize(xmlPip)) {
      initialized = true;
      subscriberTimer = new PRSubscriberTimer(contextHandlerInterface,
          subscriptions, filePath);
      timer.scheduleAtFixedRate(subscriberTimer, 0, 10 * 1000);
    } else {
      return;
    }
  }

  /**
   * Performs the effective initialization of the PIP.
   * 
   * @param xmlPip
   *          the xml of the pip in string format
   * @return true if everything goes ok, false otherwise
   */
  private boolean initialize(String string) {
    try {
      XMLPip xmlPip = JAXBUtility.unmarshalToObject(XMLPip.class, string);
      for (XMLAttribute xmlAttribute : xmlPip.getAttributes()) {
        Map<String, String> arguments = xmlAttribute.getArgs();
        if (attributeIdFilter == null) {
          attributeIdFilter = arguments.get("filter");
        }
        Attribute attribute = new Attribute();
        if (!attribute.createAttributeId(arguments.get(ATTRIBUTE_ID))) {
          LOGGER.log(Level.SEVERE, "[PIPDsa] wrong set Attribute");
          return false;
        }
        if (!attribute
            .setCategory(Category.toCATEGORY(arguments.get(CATEGORY)))) {
          LOGGER.log(Level.SEVERE,
              "[PIPDsa] wrong set category " + arguments.get(CATEGORY));
          return false;
        }
        if (!attribute.setAttributeDataType(
            DataType.toDATATYPE(arguments.get(DATA_TYPE)))) {
          LOGGER.log(Level.SEVERE, "[PIPDsa] wrong set datatype");
          return false;
        }
        addAttribute(attribute);
      }
      return true;
    } catch (JAXBException e) {
      e.printStackTrace();
      return false;
    }
  }

  // ---------------------------------------------------------------------------
  // FUNCTIONS
  // ---------------------------------------------------------------------------
  /**
   * Performs the retrieve operation.
   * <p>
   * The retrieve operation is a very basic operation in which the PIP simply
   * asks to the AttributeManager the value in which it is interested into. Once
   * that value has been retrieved, the PIP will fatten the request.
   * </p>
   * 
   * @param accessRequest
   *          this is an in/out parameter
   */
  @Override
  public void retrieve(RequestType accessRequest) throws PIPException {

    // BEGIN parameter checking
    if (accessRequest == null || !initialized || !isInitialized()) {
      LOGGER.log(Level.SEVERE, "[PIPREader] wrong initialization" + initialized
          + "\t" + isInitialized());
      return;
    }
    // END parameter checking

    String value;
    String filter = findAttributeValueForFilter(accessRequest);
    for (Attribute attribute : getAttributes()) {
      value = read(filter, attribute);

      LOGGER.log(Level.INFO, "[PIPDsa] AttributeId: " + attribute
          .getAttributeId() + " Value:" + value);

      accessRequest.addAttribute(attribute.getCategory()
          .toString(),
          attribute.getAttributeDataType().toString(),
          attribute.getAttributeId(), value);

    }

    // FIXME!!!! NOT CORRECT

    if (filter.contains("DSA-ca5e8785-d0b1-4c1e-82da-fa67436f5ff1")) {
      accessRequest.addAttribute(Category.RESOURCE.toString(),
          DataType.STRING.toString(),
          "urn:oasis:names:tc:xacml:3.0:resource:resource-classification",
          "Highly Confidential");
      /*
       * accessRequest.addAttribute(Category.SUBJECT .toString(),
       * DataType.STRING.toString(),
       * "urn:oasis:names:tc:xacml:3.0:subject:subject-ismemberof",
       * "SecurityAnalyst");
       * 
       * }
       */
    }

  }

  private String findAttributeValueForFilter(RequestType accessRequest) {
    for (AttributesType attributes : accessRequest.getAttributes()) {
      for (AttributeType attribute : attributes.getAttribute()) {
        if (attribute.getAttributeId().equals(attributeIdFilter)) {
          return (String) attribute.getAttributeValue().get(0).getContent().get(
              0);
        }
      }
    }
    return null;
  }

  /**
   * Performs the subscribe operation. This operation is very similar to the
   * retrieve operation. The only difference is that in this case we have to
   * signal to the thread in charge of performing the polling that it has to
   * poll a new attribute
   * 
   * @param accessRequest
   *          IN/OUT parameter
   */
  @Override
  public void subscribe(RequestType accessRequest) throws PIPException {
    // BEGIN parameter checking
    if (accessRequest == null || !initialized || !isInitialized()) {
      LOGGER.log(Level.SEVERE, "[PIPREader] wrong initialization" + initialized
          + "\t" + isInitialized());
      return;
    }
    // END parameter checking

    subscriberTimer.setContextHandlerInterface(contextHandlerInterface);

    if (subscriberTimer.getContextHandler() == null
        || contextHandlerInterface == null) {
      LOGGER.log(Level.SEVERE, "Context handler not set");
      return;
    }

    String value;
    String filter = findAttributeValueForFilter(accessRequest);
    for (Attribute attribute : getAttributes()) {
      value = read(filter, attribute);

      accessRequest.addAttribute(attribute.getCategory()
          .toString(),
          attribute.getAttributeDataType().toString(),
          attribute.getAttributeId(), value);

      // read the value of the attribute, if necessary extract the additional
      // info

      // add the attribute to the subscription list
      if (!subscriptions.contains(attribute)) {
        subscriptions.add(attribute);
      }
    }

  }

  @Override
  public void updateAttribute(String json) throws PIPException {
    // TODO Auto-generated method stub

  }

  /**
   * Checks if it has to remove an attribute (the one passed in the list) from
   * the list of subscribed attributes
   * 
   * @param attributes
   *          the list of attributes that must be unsubscribed
   */
  @Override
  public void unsubscribe(List<Attribute> attributes) throws PIPException {
    // BEGIN parameter checking
    if (attributes == null || !initialized || !isInitialized()) {
      LOGGER.log(Level.SEVERE, "[PIPREader] wrong initialization" + initialized
          + "\t" + isInitialized());
      return;
    }
    // END parameter checking

    for (Attribute attribute : attributes) {
      if (attribute.getAttributeId().equals(getAttributeIds().get(0))) {
        for (Attribute attributeS : subscriptions) {
          if (attributeS.getAdditionalInformations()
              .equals(attribute.getAdditionalInformations())) {
            subscriptions.remove(attributeS);
            System.out.println("UNSUB " + subscriptions.size());
            return;
          }
        }
      }
    }
  }

  /**
   * This is the function called by the context handler whenever we have a
   * remote retrieve request
   */
  @Override
  public String retrieve(Attribute attributeRetrievals) throws PIPException {
    String value;

    return null;
  }

  /**
   * This is the function called by the context handler whenever we have a
   * remote retrieve request
   */
  @Override
  public String subscribe(Attribute attributeRetrieval) throws PIPException {

    subscriberTimer.setContextHandlerInterface(contextHandlerInterface);

    if (subscriberTimer.getContextHandler() == null
        || contextHandlerInterface == null) {
      LOGGER.log(Level.SEVERE, "Context handler not set");
      return null;
    }

    if (!subscriptions.contains(attributeRetrieval)) {
      subscriptions.add(attributeRetrieval);
    }
    return null;

  }

  @Override
  public void retrieve(RequestType request,
      List<Attribute> attributeRetrievals) {
    LOGGER.log(Level.SEVERE, "Wrong method called");
    return;

  }

  @Override
  public void subscribe(RequestType request,
      List<Attribute> attributeRetrieval) {
    LOGGER.log(Level.SEVERE, "Wrong method called");
    return;

  }

  @Override
  public void performObligation(ObligationInterface obligation) {
    // TODO Auto-generated method stub

  }

  /**
   * Reads the file looking for the line containing the filter we are passing as
   * argument and the role stated as other parameter
   * 
   * <br>
   * NOTE we suppose that in the file each line has the following structure:
   * filter\tattribute.
   * 
   * @param filter
   *          the string to be used to search for the item we're interested into
   * @param role
   *          the role of the string
   * @return the string or null
   * 
   * 
   * @throws PIPException
   */
  private String read(String filter, Attribute attribute) throws PIPException {
    String url = null;
    RestTemplate restTemplate = new RestTemplate();
    if (attribute.getAttributeId().contains("version")) {
      url = "https://dsamgrc3isp.iit.cnr.it/DSAAPI/fetchdsaversion";
    } else if (attribute.getAttributeId().contains("status")) {
      url = "https://dsamgrc3isp.iit.cnr.it/DSAAPI/getstatuscnr";
    } else {
      return "1";
    }
    try {
      HttpHeaders headers = new HttpHeaders();
      headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
      HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
      UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url)
          .queryParam("dsaid", filter);
      Message message = restTemplate.postForObject(builder.build().toString(),
          filter, Message.class);
      return message.getMessage();
    } catch (Exception ioException) {
      throw new PIPException(ioException.getMessage());
    }
  }

  // ---------------------------------------------------------------------------
  // SETTERS
  // ---------------------------------------------------------------------------

}
