package it.cnr.iit.ucs.test;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.assertj.core.util.Files;
import org.junit.Test;

import iit.cnr.it.ucsinterface.contexthandler.STATUS;
import iit.cnr.it.ucsinterface.pdp.PDPEvaluation;
import iit.cnr.it.usagecontrolframework.configuration.xmlclasses.XMLPdp;
import iit.cnr.it.usagecontrolframework.pdp.PolicyDecisionPoint;
import oasis.names.tc.xacml.core.schema.wd_17.ResponseType;

public class PolicyDecisionPointTest {

  PolicyDecisionPoint policyDecisionPoint = new PolicyDecisionPoint(
      new XMLPdp());

  @Test
  public void policyTest() {
    String policy = Files.contentOf(new File(PolicyDecisionPointTest.class
        .getClassLoader().getResource("Policy_double.xml").getFile()), "UTF-8");
    String request = Files.contentOf(new File(PolicyDecisionPointTest.class
        .getClassLoader().getResource("RequestAntonio.xml").getFile()),
        "UTF-8");

    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(policy);

    PDPEvaluation pdpResponse = policyDecisionPoint.evaluate(request,
        stringBuilder, STATUS.TRYACCESS);
    assertTrue(policyDecisionPoint.getRuleId((ResponseType) pdpResponse
        .getResponseAsObject()).equals("rule-permit"));
  }

  @Test
  public void policyTestFirstNA() {
    String policy = Files.contentOf(new File(PolicyDecisionPointTest.class
        .getClassLoader().getResource("Policy_double_first_NA.xml").getFile()),
        "UTF-8");
    String request = Files.contentOf(new File(PolicyDecisionPointTest.class
        .getClassLoader().getResource("RequestAntonio.xml").getFile()),
        "UTF-8");

    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(policy);

    PDPEvaluation pdpResponse = policyDecisionPoint.evaluate(request,
        stringBuilder, STATUS.TRYACCESS);
    assertTrue(policyDecisionPoint.getRuleId((ResponseType) pdpResponse
        .getResponseAsObject()).equals("rule-permit_1"));
  }

  @Test
  public void policyTestDeny() {
    String policy = Files.contentOf(new File(PolicyDecisionPointTest.class
        .getClassLoader().getResource("Policy_double.xml").getFile()), "UTF-8");
    String request = Files.contentOf(new File(PolicyDecisionPointTest.class
        .getClassLoader().getResource("RequestDeny.xml").getFile()),
        "UTF-8");

    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(policy);

    PDPEvaluation pdpResponse = policyDecisionPoint.evaluate(request,
        stringBuilder, STATUS.TRYACCESS);
    assertTrue(policyDecisionPoint.getRuleId((ResponseType) pdpResponse
        .getResponseAsObject()).equals("urn:oasis:names:tc:xacml:3.0:defdeny"));
  }

}
