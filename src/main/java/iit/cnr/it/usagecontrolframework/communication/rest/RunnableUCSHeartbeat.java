package iit.cnr.it.usagecontrolframework.communication.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;

import iit.cnr.it.types.Clock;

public class RunnableUCSHeartbeat implements Runnable {

	private Clock clock = Clock.CLOCK;

	@Value("${clock.timeWindow}")
	private int timeWindow;

	@Value("${clock.maxMissedHeartbeat}")
	private int maxMissedHeartbeat;

	private final static Logger LOGGER = LoggerFactory.getLogger(RunnableUCSHeartbeat.class);

	public RunnableUCSHeartbeat() {
	}

	@Override
	public void run() {
		try {

			init();

			while (true) {

				synchronized (Clock.getThreadLock()) {
					while (!clock.isHasHeartbeatArrived()) {
						Clock.getThreadLock().wait(clock.getTimeWindow() * 1000);
						if (!clock.isHasHeartbeatArrived()) {
							clock.setMissedHeartbeat(clock.getMissedHeartbeat() + 1);

							if (clock.getMissedHeartbeat() >= clock.getMaxMissedHeartbeat()) {
								// clock.setMissedHeartbeat(0);

								if (!clock.isDead()) {
									clock.setDead(true);
									clock.setDeadCount(clock.getDeadCount() + 1);
								}

								LOGGER.error("SUCA:> The IAI is dead");
								LOGGER.error("SUCA:> Number of time that the PEP is dead: " + clock.getDeadCount());
							}
						}
					}
				}

				LOGGER.info("SUCA:> Heartbeat arrived from IAI!");
				init();
				// clock.setMissedHeartbeat(0);
				// clock.setHasHeartbeatArrived(false);
				clock.setResponse(ResponseEntity.accepted().body("SUCA:> Heartbeat received"));

				synchronized (Clock.getApiLock()) {
					Clock.getApiLock().notify();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void init() {
		clock.setMissedHeartbeat(0);
		clock.setTimeWindow(timeWindow);
		clock.setMaxMissedHeartbeat(maxMissedHeartbeat);
		clock.setDead(false);
		clock.setHasHeartbeatArrived(false);
	}

}
