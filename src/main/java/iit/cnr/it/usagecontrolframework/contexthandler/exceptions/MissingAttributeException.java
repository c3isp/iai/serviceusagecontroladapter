package iit.cnr.it.usagecontrolframework.contexthandler.exceptions;

public class MissingAttributeException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MissingAttributeException(String string) {
		super(string);
	}
	
}
