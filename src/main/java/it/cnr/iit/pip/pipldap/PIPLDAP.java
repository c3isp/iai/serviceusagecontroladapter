/*
 * CNR - IIT (2015-2016)
 *
 * @authors Fabio Bindi and Filippo Lauria
 */
package it.cnr.iit.pip.pipldap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Value;

import iit.cnr.it.ucsinterface.obligationmanager.ObligationInterface;
import iit.cnr.it.ucsinterface.pip.PIPBase;
import iit.cnr.it.ucsinterface.pip.exception.PIPException;
import iit.cnr.it.usagecontrolframework.configuration.xmlclasses.XMLAttribute;
import iit.cnr.it.usagecontrolframework.configuration.xmlclasses.XMLPip;
import iit.cnr.it.xacmlutilities.Attribute;
import iit.cnr.it.xacmlutilities.AttributeId;
import iit.cnr.it.xacmlutilities.Category;
import iit.cnr.it.xacmlutilities.DataType;
import iit.cnr.it.xacmlutilities.policy.utility.JAXBUtility;
import oasis.names.tc.xacml.core.schema.wd_17.RequestType;

/**
 * PIP LDAP implementation: it performs PIP operations by retrieving and
 * updating user informations stored in an LDAP server
 *
 * @author Fabio Bindi and Filippo Lauria
 */
public class PIPLDAP extends PIPBase {

	private final static Logger LOGGER = Logger.getLogger(PIPLDAP.class.getName());

	@Value("${ldap.domainComponent}")
	private String ldapDc;

	protected final BlockingQueue<Attribute> subscriptions = new LinkedBlockingQueue<>();
	private SubscriberTimer subscriberTimer;
	private volatile boolean initialized = false;
	private Category expectedCategory;
	private LDAPConnector ldapConnector = null;
	private Timer timer = new Timer();

	public PIPLDAP(String xmlPip) {
		super(xmlPip);
		if (!isInitialized()) {
			return;
		}
		if (initialize(xmlPip)) {
			initialized = true;
			subscriberTimer = new SubscriberTimer(contextHandlerInterface, subscriptions, ldapConnector,
					getAttributeIds());
			timer.scheduleAtFixedRate(subscriberTimer, 0, 10 * 1000);
		} else {
			return;
		}
	}

	private boolean initialize(String xmlPIP) {
		try {
			XMLPip xmlPip = JAXBUtility.unmarshalToObject(XMLPip.class, xmlPIP);
			List<XMLAttribute> attributes = xmlPip.getAttributes();
			for (XMLAttribute xmlAttribute : attributes) {
				Map<String, String> arguments = xmlAttribute.getArgs();
				Attribute attribute = new Attribute();
				if (!attribute.createAttributeId(arguments.get(ATTRIBUTE_ID))) {
					LOGGER.log(Level.SEVERE, "[PIPReader] wrong set Attribute");
					return false;
				}
				if (!attribute.setCategory(Category.toCATEGORY(arguments.get(CATEGORY)))) {
					LOGGER.log(Level.SEVERE, "[PIPReader] wrong set category " + arguments.get(CATEGORY));
					return false;
				}
				if (!attribute.setAttributeDataType(DataType.toDATATYPE(arguments.get(DATA_TYPE)))) {
					LOGGER.log(Level.SEVERE, "[PIPReader] wrong set datatype");
					return false;
				}
				if (attribute.getCategory() != Category.ENVIRONMENT) {
					if (!setExpectedCategory(arguments.get(EXPECTED_CATEGORY))) {
						return false;
					}
				}
				if (ldapConnector == null) {
					if (setLdapConnector(arguments.get("HOST"), arguments.get("BNDDN"),
							arguments.get("PASSWORD")) == -1) {
						return false;
					}
				}
				addAttribute(attribute);
			}
			return true;
		} catch (JAXBException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Sets up the conection with LDAP
	 * 
	 * @param host     the host
	 * @param bnddn
	 * @param password the password
	 */
	private int setLdapConnector(String host, String bnddn, String password) {
		// BEGIN parameter checking
		if (host == null || bnddn == null || password == null) {
			return 0;
		}
		// END parameter checking
		else {
			ldapConnector = new LDAPConnector(host, bnddn);
			try {
				ldapConnector.authenticate(password);
				return 1;
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return -1;
			}
		}
	}

	final private boolean setExpectedCategory(String category) {
		// BEGIN parameter checking
		if (!isInitialized() || category == null || category.isEmpty()) {
			initialized = false;
			return false;
		}
		// END parameter checking
		Category categoryObj = Category.toCATEGORY(category);
		if (categoryObj == null) {
			initialized = false;
			return false;
		}
		expectedCategory = categoryObj;
		return true;
	}

	/**
	 * Subscribes an user to monitor its mutable attributes by storing in a map his
	 * search base (used to access his information in the LDAP server) and the list
	 * of his mutable attributes (retrieved from the configuration file)
	 * 
	 * @param request a XACML file where the user to subscribe will be retrieved
	 * @throws PIPException
	 */
	@Override
	public synchronized void subscribe(RequestType accessRequest) throws PIPException {

		// BEGIN parameter checking
		if (accessRequest == null || !initialized || !isInitialized()) {
			LOGGER.log(Level.SEVERE, "[PIPREader] wrong initialization" + initialized + "\t" + isInitialized());
			return;
		}
		// END parameter checking

		subscriberTimer.setContextHandlerInterface(contextHandlerInterface);

		if (subscriberTimer.getContextHandler() == null || contextHandlerInterface == null) {
			LOGGER.log(Level.SEVERE, "Context handler not set");
			return;
		}

		List<String> searchAttributesList = getAttributeIds();
		List<String> attributeNames = retrieveAttributeName(searchAttributesList);

		Map<String, Set<String>> ldapMap;
		String filter;
		if (EXPECTED_CATEGORY == null) {
			return;
		} else {
			filter = accessRequest.extractValue(expectedCategory);
			ldapMap = ldapConnector.search(ldapDc, attributeNames, filter);
		}

		for (Map.Entry<String, Set<String>> entry : ldapMap.entrySet()) {
			String label = entry.getKey();
			Iterator<String> it = entry.getValue().iterator();
			for (Attribute value : getAttributes()) {
				if (new AttributeId(value.getAttributeId()).getSplittedAttribute().equalsIgnoreCase(label)) {
					while (it.hasNext()) {
						String ldapValue = it.next();
						value.createAttributeValues(value.getAttributeDataType(), ldapValue);
					}
					for (Map.Entry<String, List<String>> attributeEntry : value.getAttributeValueMap().entrySet()) {
						accessRequest.addAttribute(value.getCategory().toString(), entry.getKey(),
								value.getAttributeId(), attributeEntry.getValue());

					}
				}
				if (!subscriptions.contains(value)) {
					subscriptions.add(value);

				}
			}
		}

		// add the attribute to the access request

		// add the attribute to the subscription list

	}

	/**
	 * Retrieves the attribute values for a certain user from the LDAP server. It
	 * needs autenthication to access to the LDAP server.
	 * 
	 * @param request a XACML file where the user will be retrieved
	 * @throws PIPException
	 */
	@Override
	public void retrieve(RequestType accessRequest) throws PIPException {
		ArrayList<String> pilotList = ldapConnector.getPilots();
		String userIdentifier = accessRequest.extractValue(expectedCategory);

		String pilotName = ldapConnector.getPilotName(pilotList, userIdentifier);
		String topPilotName = ldapConnector.getTopPilotName(pilotList, userIdentifier);

		System.out.println("Identity: " + userIdentifier);
		System.out.println("Pilot Name: " + pilotName);
		System.out.println("Top Pilot Name: " + topPilotName);

		String schema = null;
		if (pilotName != null)
			schema = ldapConnector.getSchemaForUser(pilotName, userIdentifier);
		System.out.println("Schema: " + schema);

		List<Attribute> attributes = retrieveUserAttributesFromLdap(schema, userIdentifier, pilotName, topPilotName);
		for (Attribute attribute : attributes) {

			LOGGER.log(Level.INFO, "[PIPLdap] AttributeId: " + attribute.getAttributeId() + " for " + userIdentifier
					+ " Value:" + attribute.getAttributeValues(DataType.STRING));

			accessRequest.addAttribute(attribute.getCategory().toString(), DataType.STRING.toString(),
					attribute.getAttributeId(), attribute.getAttributeValues(DataType.STRING));
		}
		return;
	}

	private List<Attribute> retrieveUserAttributesFromLdap(String schema, String userIdentifier, String pilotName,
			String topPilotName) {
		List<Attribute> attributesList = new ArrayList<>();
		ArrayList<String> membership = new ArrayList<>();
		ArrayList<String> organization = new ArrayList<>();
		ArrayList<String> country = new ArrayList<>();

		if (schema != null) {
			Map<String, Set<String>> attributes = getAttributes(extractValueFromSchema(schema, "cn="), userIdentifier,
					pilotName, topPilotName);
			membership = getMembership(attributes);
			organization = getOrganization(attributes);
			country = getCountry(attributes);
		} else {
			ArrayList<String> nullString = new ArrayList<>();
			nullString.add("");
			membership = nullString;
			organization = nullString;
			country = nullString;
		}

		System.out.println("Membership: " + membership);
		System.out.println("Organization: " + organization);
		System.out.println("country: " + country);

		for (String attributeId : getAttributeIds()) {
			if (attributeId.contains("member")) {
				Attribute attribute = new Attribute();
				attribute.createAttributeId(attributeId);
				attribute.setAttributeDataType(DataType.STRING);
				attribute.setCategory(Category.SUBJECT);
				HashMap<String, ArrayList<String>> tmp = new HashMap<String, ArrayList<String>>();
				tmp.put(DataType.STRING.toString(), membership);
				attribute.setAttributevalueMap(tmp);
				attributesList.add(attribute);
			}
			if (attributeId.contains("organisation")) {
				Attribute attribute = new Attribute();
				attribute.createAttributeId(attributeId);
				attribute.setAttributeDataType(DataType.STRING);
				attribute.setCategory(Category.SUBJECT);
				HashMap<String, ArrayList<String>> tmp = new HashMap<String, ArrayList<String>>();
				tmp.put(DataType.STRING.toString(), organization);
				attribute.setAttributevalueMap(tmp);
				attributesList.add(attribute);
			}
			if (attributeId.contains("country")) {
				Attribute attribute = new Attribute();
				attribute.createAttributeId(attributeId);
				attribute.setAttributeDataType(DataType.STRING);
				attribute.setCategory(Category.SUBJECT);
				HashMap<String, ArrayList<String>> tmp = new HashMap<String, ArrayList<String>>();
				tmp.put(DataType.STRING.toString(), country);
				attribute.setAttributevalueMap(tmp);
				attributesList.add(attribute);
			}
		}
		return attributesList;

	}

	private String extractValueFromSchema(String schema, String value) {
		return schema.split(value)[1].split(",")[0];
	}

	private Map<String, Set<String>> getAttributes(String user, String userIdentifier, String pilotName,
			String topPilotName) {
		Map<String, Set<String>> attributes = null;
		// It tries to extract the role and the organization with the standard LDAP
		// structure
		String cnName = ldapConnector.getCnInSubpilot(userIdentifier, pilotName, topPilotName);
		attributes = ldapConnector.roleInSubPilot(user, pilotName, topPilotName);
		attributes.put("organization", (ldapConnector.companySubpilot(userIdentifier, pilotName)));
		attributes.put("country", (ldapConnector.getCountry(cnName)));
		return attributes;
	}

	private ArrayList<String> getMembership(Map<String, Set<String>> attributes) {
		ArrayList<String> values = new ArrayList<>();
		if (attributes.containsKey("role")) {
			values.addAll(attributes.get("role"));
		} else {
			values.add("empty");
		}

		return values;
	}

	private ArrayList<String> getCountry(Map<String, Set<String>> attributes) {
		ArrayList<String> values = new ArrayList<>();
		if (attributes.containsKey("country")) {
			values.addAll(attributes.get("country"));
		} else {
			values.add("empty");
		}
		return values;
	}

	private ArrayList<String> getOrganization(Map<String, Set<String>> attributes) {
		ArrayList<String> values = new ArrayList<>();
		if (attributes.containsKey("organization")) {
			values.addAll(attributes.get("organization"));
		} else {
			values.add("empty");
		}
		return values;
	}

	private List<String> retrieveAttributeName(List<String> searchAttributesList) {
		List<String> attributeNames = new LinkedList<>();

		for (String string : searchAttributesList) {
			AttributeId attributeId = new AttributeId(string);
			attributeNames.add(attributeId.getSplittedAttribute());
		}

		// attributeNames.add("mail");
		return attributeNames;
	}

	@Override
	public void unsubscribe(List<Attribute> attributes) throws PIPException {
		// TODO Auto-generated method stub

	}

	@Override
	public String retrieve(Attribute attributeRetrievals) throws PIPException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String subscribe(Attribute attributeRetrieval) throws PIPException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void retrieve(RequestType request, List<Attribute> attributeRetrievals) {
		// TODO Auto-generated method stub

	}

	@Override
	public void subscribe(RequestType request, List<Attribute> attributeRetrieval) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAttribute(String json) throws PIPException {
		// TODO Auto-generated method stub

	}

	@Override
	public void performObligation(ObligationInterface obligation) {
		// TODO Auto-generated method stub

	}
}
